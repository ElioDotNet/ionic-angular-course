import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  Input
} from '@angular/core';

import {
  Plugins,
  Capacitor,
  CameraSource,
  CameraResultType
} from '@capacitor/core';

import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss']
})
export class ImagePickerComponent implements OnInit {
  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  @Output() imagePick = new EventEmitter<string | File>();
  @Input() showPreview = false;
  selectedImage: string;
  usePicker = false;

  constructor(private platform: Platform) {}

  ngOnInit() {
    // console.log('Mobile:', this.platform.is('mobile'));
    // console.log('Hybrid:', this.platform.is('hybrid'));
    // console.log('iOS:', this.platform.is('ios'));
    // console.log('Android:', this.platform.is('android'));
    // console.log('Desktop:', this.platform.is('desktop'));
    if (
      // L'applicazione sta girando nel browser
      (this.platform.is('mobile') && !this.platform.is('hybrid')) ||
      this.platform.is('desktop')
    ) {
      this.usePicker = true;
    }
  }

  async onPickImage() {
    // Non è disponibile la fotocamera oppure siamo all'interno di un browser
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.filePickerRef.nativeElement.click(); // si genera evento click dell'elemento(apre la ricerca del file)
      return;
    }
    await Plugins.Camera.getPhoto({
      quality: 50, // va da 1 a 100(più grande è più la foto occupa spazio)
      source: CameraSource.Prompt, // si può scegliere tra la galleria di immagini oppure di scattare una foto
      correctOrientation: true, // ruota automaticamente l'immagine per corregere l'orientamento
      // height: 320,
      width: 300,
      resultType: CameraResultType.Base64 // l'immagine è codificata in una stringa ma si può codificare in un file
    })
      .then(image => {
        // Si restuituisce l'immagine in formato stringa
        this.selectedImage = image.base64String;
        this.imagePick.emit(image.base64String);
      })
      .catch(error => {
        console.log(error);
        if (this.usePicker) {
          this.filePickerRef.nativeElement.click(); // si genera evento click dell'elemento(apre la ricerca del file)
        }
        return false;
      });
  }

  onFileChosen(event: Event) {
    const pickedFile = (event.target as HTMLInputElement).files[0];
    if (!pickedFile) {
      return;
    }
    const fr = new FileReader();
    fr.onload = () => { // si scatena quando viene letto il file
      const dataUrl = fr.result.toString();
      this.selectedImage = dataUrl;
      this.imagePick.emit(pickedFile);
    };
    fr.readAsDataURL(pickedFile); // lo converte in una stringa base64
  }
}
