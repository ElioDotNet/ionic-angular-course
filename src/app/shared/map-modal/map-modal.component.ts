import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from './../../../environments/environment';

// Open Layers Map
// import Map from 'ol/Map';
// import Tile from 'ol/layer/Tile';
// import OSM from 'ol/source/OSM';
// import View from 'ol/View';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss'],
})
export class MapModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('map', {static: false}) mapElementRef: ElementRef;
  @Input() center = { lat: -34.397, lng: 150.644 };
  @Input() selectable = true;
  @Input() closeButtonText = 'Cancel';
  @Input() title = 'Pick Location';
  clickListener: any;
  googleMaps: any;

  constructor(private modalCtrl: ModalController,
              private renderer: Renderer2) { }

  ngOnInit() {}

  ngAfterViewInit() {
    // *** OPEN LAYERS MAP ***
    // const map = new Map({
    //   target: this.mapElementRef.nativeElement,
    //   layers: [
    //     new Tile({
    //       source: new OSM()
    //     })
    //   ],
    //   view: new View({
    //     center: [150.644, -34.397],
    //     zoom: 16
    //   })
    // });

    // GOOGLE MAPS
    this.getGoogleMaps()
        .then(googleMaps => {
          this.googleMaps = googleMaps;
          const mapEl = this.mapElementRef.nativeElement;
          const map = new googleMaps.Map(mapEl, {
            center: this.center,
            zoom: 16
          });

          this.googleMaps.event.addListenerOnce(map, 'idle', () => {
            this.renderer.addClass(mapEl, 'visible');
          });

          if (this.selectable) {
            // Si fa click sulla mappa
            this.clickListener = map.addListener('click', event => {
              const selectedCoords = {
                lat: event.latLng.lat,
                lng: event.latLng.lng
              };
              this.modalCtrl.dismiss(selectedCoords);
            });
          } else {
            const marker = new googleMaps.Marker({
              position: this.center,
              map,
              title: 'Picked Location'
            });
            marker.setMap(map);
          }
        })
        .catch(err => {
          console.log(err);
        });
  }

  onCancel() {
      this.modalCtrl.dismiss();
  }

  ngOnDestroy() {
    // Si rimuove il listener per evitare il memory leak
    if (this.clickListener) {
      this.googleMaps.event.removeListener(this.clickListener);
    }
  }

  private getGoogleMaps(): Promise<any> {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }
    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src =
          `https://maps.googleapis.com/maps/api/js?key=${environment.googleMapsAPIKey}`;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const loadedGoogleModule = win.google;
        if (loadedGoogleModule && loadedGoogleModule.maps) {
          resolve(loadedGoogleModule.maps);
        } else {
          reject('Google maps SDK not available.');
        }
      };
    });
  }
}
