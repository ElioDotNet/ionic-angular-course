import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';

import { Observable, of } from 'rxjs';
import { take, tap, switchMap } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  // implementiamo CanLoad anzichè CanActivate perè nel routing usiamo il lazy loading
  constructor(private authService: AuthService, private router: Router) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.userIsAuthenticated.pipe(
      take(1),
      switchMap(isAuthenticated => {
        if (!isAuthenticated) { // se non siamo autenticati si fa l'autologin
          return this.authService.autoLogin();
        } else {
          return of(isAuthenticated);
        }
      }),
        tap(isAuthenticated => {
            if (!isAuthenticated) {
                this.router.navigateByUrl('/auth');
            }
        })
    );
  }
}
