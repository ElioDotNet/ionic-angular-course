import { environment } from './../../environments/environment';
/* tslint:disable:variable-name */
import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Plugins } from '@capacitor/core';

import { User } from './user.model';

import { BehaviorSubject, from } from 'rxjs';
import { map, tap } from 'rxjs/operators';

// Formato dati che ritorna la post a Firebase
export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  localId: string; // userId creato da Firebase
  expiresIn: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  private _user = new BehaviorSubject<User>(null);
  private activeLogoutTimer: any;

  constructor(private http: HttpClient) {}

  get userIsAuthenticated() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return !!user.token; // !! converte in boolean
        } else {
          // se non c'è il token l'utente non è autorizzato
          return false;
        }
      })
    );
  }

  get userId() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.id;
        } else {
          return null;
        }
      })
    );
  }

  get token() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.token;
        } else {
          return null;
        }
      })
    );
  }

  autoLogin() {
    return from(Plugins.Storage.get({ key: 'authData' })) // l'operatore from accetta una promise e restituisce un observable
      .pipe(map(storedData => {
        if (!storedData || !storedData.value) { // non ci sono valori salvati
          return null;
        }
        const parsedData = JSON.parse(storedData.value) as {
          token: string;
          userId: string;
          tokenExpirationDate: string;
          email: string;
        };
        const expirationTime = new Date(parsedData.tokenExpirationDate);
        if (expirationTime <= new Date()) { // il token è scaduto
          return null;
        }
        return new User(
          parsedData.userId,
          parsedData.email,
          parsedData.token,
          expirationTime
        );
      }),
      tap(user => {
        if (!user) {
          this._user.next(user); // abbiamo già lo User loggato
          this.autoLogout(user.tokenDuration);
        }
      }),
      map(user => {
        return !!user; // se abbiamo già lo user, ritorna true, diversamente false
      })
    );
  }

  signUp(email: string, password: string) {
    return this.http
      .post<AuthResponseData>(
        `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${environment.firebaseAPIKey}`,
        { email, password, returnSecureToken: true }
      )
      .pipe(
        tap(this.setUserData.bind(this)) // viene passato un riferimento a questa funzione
      );
  }

  login(email: string, password: string) {
    return this.http.post<AuthResponseData>(
      `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${environment.firebaseAPIKey}`,
      { email, password, returnSecureToken: true }
    )
    .pipe(
      tap(this.setUserData.bind(this)) // viene passato un riferimento a questa funzione
    );
  }

  logout() {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer); // resetta il timer
    }
    this._user.next(null); // resettiamo lo user a null
    Plugins.Storage.remove({ key: 'authData' }); // si rimuovono i dati nello storage
  }

  // Effettua un logout automatico, quando il token scade
  private autoLogout(duration: number) {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer); // resetta il timeout
    }
    this.activeLogoutTimer = setTimeout(() => { // imposta un timer della durata del token
      this.logout();
    }, duration);
  }

  ngOnDestroy() {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer); // resetta il timer
    }
  }

  private setUserData(userData: AuthResponseData) {
     // Calcolo scadenza token in millisecondi
     const expirationDate = new Date(
      new Date().getTime() + +userData.expiresIn * 1000
    );
     const user = new User(
        userData.localId,
        userData.email,
        userData.idToken,
        expirationDate
      );

     this._user.next(user);
     this.autoLogout(user.tokenDuration);
     this.storeAuthData(
      userData.localId,
      userData.idToken,
      expirationDate.toISOString(),
      userData.email
    );
  }

  // Salvataggio dati di login per poi poterli riutilizzare
  private storeAuthData(
    userId: string,
    token: string,
    tokenExpirationDate: string,
    email: string
  ) {
    const data = JSON.stringify({
      userId,
      token,
      tokenExpirationDate,
      email
    });
    Plugins.Storage.set({ key: 'authData', value: data }); // si usa Capacitor per il salvataggio dati
  }
}
