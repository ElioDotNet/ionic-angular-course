import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';

import { Observable } from 'rxjs';

import { AuthService, AuthResponseData } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage {
  isLoading = false;
  isLogin = true;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {}

  authenticate(email: string, password: string) {
    this.isLoading = true;
    // Impostazioni per il Loading Controller
    this.loadingCtrl
    .create({ keyboardClose: true, message: 'Loading in...' })
    .then(loadingEl => {
      loadingEl.present(); // apre il loading
      let authObs: Observable<AuthResponseData>;
      // Si differenziano gli observable a seconda se stamo facendo una Login o una SignUp
      if (this.isLogin) {
        authObs = this.authService.login(email, password);
      } else {
        authObs = this.authService.signUp(email, password);
      }
      authObs.subscribe(
          () => {
            this.isLoading = false;
            loadingEl.dismiss(); // chiude il loading dopo la login
            this.router.navigateByUrl('/places/tabs/discover');
          },
          errRes => {
            loadingEl.dismiss(); // chiude il loading
            const code = errRes.error.error.message;
            // Decodifica dei messaggi restituiti dall'oggetto error
            let message = 'Could not sign you up, please try again.';
            if (code === 'EMAIL_EXISTS') {
              message = 'This email address already exists!';
            } else if (code === 'EMAIL_NOT_FOUND') {
              message = 'E-Mail address could not be found.';
            } else if (code === 'INVALID_PASSWORD') {
              message = 'This password is not correct.';
            }
            this.showAlert(message);
          }
        );
      });
  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;

    this.authenticate(email, password);
    form.reset(); // i dati del form vengono puliti
  }

  private showAlert(message: string) {
    this.alertCtrl
      .create({
        header: 'Authentication failed',
        message,
        buttons: ['Ok']
      })
      .then(alertEl => alertEl.present());
  }
}
