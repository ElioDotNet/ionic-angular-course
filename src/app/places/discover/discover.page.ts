import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';

import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Place } from '../place.model';
import { PlacesService } from '../places.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit, OnDestroy {
  loadedPlaces: Place[];
  listedLoadedPlaces: Place[];
  relevantPlaces: Place[];
  isLoading = false;
  private placeSub: Subscription;
  private filter = 'all';

  constructor(
      private placesService: PlacesService,
      private menuCtrl: MenuController,
      private authService: AuthService,
    ) { }

    ngOnInit() {
      this.placeSub = this.placesService.places.subscribe(places => {
        this.loadedPlaces = places;
        this.onFilterUpdate(this.filter);
      });
    }

    ionViewWillEnter() {
      this.isLoading = true;
      this.placesService.fetchPlaces().subscribe(() => {
          this.isLoading = false;
        });
    }

    ngOnDestroy() {
      if (this.placeSub) {
        this.placeSub.unsubscribe();
      }
    }

    // onOpenMenu() {
    //     this.menuCtrl.toggle('m1'); // apre il menù o se già aperto lo chiude
    // }

  onFilterUpdate(filter: string) {
    this.authService.userId.pipe(take(1)).
      subscribe(userId => {
        const isShown = (place: { userId: string; }) => filter === 'all' || place.userId !== userId;
        this.relevantPlaces = this.loadedPlaces.filter(isShown);
        this.filter = filter;
        this.listedLoadedPlaces = this.relevantPlaces.slice(1);
      });
  }

  // onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
  //   if (event.detail.value === 'all') {
  //     this.relevantPlaces = this.loadedPlaces;
  //   } else {
  //     // Tutte le prenotazioni escluse quelle dello user corrente
  //     this.relevantPlaces = this.loadedPlaces.filter(place => place.userId !== this.authService.UserId);
  //   }
  //   this.listedLoadedPlaces = this.relevantPlaces.slice(1);
  // }
}
