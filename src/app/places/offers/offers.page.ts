import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PlacesService } from '../places.service';
import { Place } from '../place.model';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  offers: Place[];
  private placeSub: Subscription;
  isLoading = false;

  constructor(private placesService: PlacesService, private router: Router) { }
      /* Possiamo in alternativa al Subject utilizzare 'ionViewWillEnter' evento gestito
       da Ionic, perchè ngOnInit si verifica solo alla creazione del component */
      // ionViewWillEnter() {
      //   this.offers = this.placesService.places;
      // }

    ionViewWillEnter() {
      this.isLoading = true;
      this.placesService.fetchPlaces().subscribe(() => {
        this.isLoading = false;
      });
    }

    ngOnInit() {
      this.placeSub = this.placesService.places.subscribe(places => {
        this.offers = places;
      });
    }

    ngOnDestroy() {
      if (this.placeSub) {
        this.placeSub.unsubscribe();
      }
    }

    onEdit(offerId: string, slidingItem: IonItemSliding) {
      slidingItem.close(); // chiudiamo lo slide
      this.router.navigate(['/', 'places', 'tabs', 'offers', 'edit', offerId]);
    }

}
