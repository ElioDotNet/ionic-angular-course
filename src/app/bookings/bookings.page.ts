import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonItemSliding, LoadingController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { BookingsService } from './bookings.service';
import { Booking } from './booking.model';

@Component({
    selector: 'app-bookings',
    templateUrl: './bookings.page.html',
    styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
    isLoading = false;
    loadedBookings: Booking[];
    bookingSub: Subscription;

    constructor(
        private bookingsService: BookingsService,
        private loadingCtrl: LoadingController
    ) { }

    ngOnInit() {
        this.bookingSub = this.bookingsService.bookings.subscribe(bookings => {
            this.loadedBookings = bookings;
        });
    }

    ionViewWillEnter() {
        this.isLoading = true;
        this.bookingsService.fetchBookings().subscribe(() => {
            this.isLoading = false;
        });
    }

    ngOnDestroy() {
        if (this.bookingSub) {
            this.bookingSub.unsubscribe();
        }
    }

    onCancelBooking(bookingId: string, slidingBooking: IonItemSliding) {
        slidingBooking.close();
        this.loadingCtrl.create({ message: 'Cancelling' }).then(loadindEl => {
            loadindEl.present();
            this.bookingsService.cancelBooking(bookingId).subscribe(() => {
                loadindEl.dismiss();
            });
        });
    }
}
